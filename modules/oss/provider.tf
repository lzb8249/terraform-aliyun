
variable "profile" {
  default = "default"
}

variable "region" {
  default = "cn-shanghai"
}

provider "alicloud" {
  version = ">= 1.90.0"
  region  = var.region
  profile = var.profile
}

terraform {
  required_version = ">= 0.12.8"
}

