
variable "network" {
  default = "10.80.0.0/16"
}

variable "subnets" {
  default = {
    demo    = "10.80.8.0/21"
    prod    = "10.80.16.0/21"
    k8s     = "10.80.32.0/21"
  }
}


variable "elvin_demo_subnets" {
  default = {
    a = "10.80.8.0/24"
    b = "10.80.9.0/24"
    c = "10.80.10.0/24"
    d = "10.80.11.0/24"
    e = "10.80.12.0/24"
    f = "10.80.13.0/24"
  }
}

variable "elvin_prod_subnets" {
  default = {
    a = "10.80.16.0/24"
    b = "10.80.17.0/24"
    c = "10.80.18.0/24"
    d = "10.80.19.0/24"
    e = "10.80.20.0/24"
    f = "10.80.21.0/24"
  }
}

variable "elvin_k8s_subnets" {
  default = {
    a = "10.80.32.0/24"
    b = "10.80.33.0/24"
    c = "10.80.34.0/24"
    d = "10.80.35.0/24"
    e = "10.80.36.0/24"
    f = "10.80.38.0/24"
  }
}

