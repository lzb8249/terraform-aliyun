output "ids" {
  value = "${alicloud_instance.instance.*.id}"
}

output "ip" {
  value = "${alicloud_instance.instance.*.private_ip}"
}

output "eip" {
  value = "${alicloud_instance.instance.*.public_ip}"
}

output "host" {
  value = "${alicloud_instance.instance.*.instance_name}"
}
