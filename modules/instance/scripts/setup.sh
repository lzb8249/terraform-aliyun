#!/bin/bash
#used for ubuntu os

add_keys() {
  ssh_dir='/root/.ssh'
  authorized_keys="${ssh_dir}/authorized_keys"
  mkdir -p ${ssh_dir}
  cli='ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC0QycANBxsXxs3fsP/molpeoo3X/66z+kFxLH/15PyzFzDK+x3eEu+1H077R+Bh8mGxTGRe2nM3LXNsITmr4OS8nrFxJJA9nEER3HDd6dtSKBhRvdynccd11t2HuOAGCb/2X/SZnKeodv+VqRBn4Yqltb0/gNlVynLlueyueIJGcavkUlZ6R1sI8gZRzr050m40CQRBFgwTLbtamX/ZI9iDGtYF7jkKy97ipvI6UXSB5OS3fhh/Ysr5OoScNkbF4UaPfrufRNWynxw2GOKDKo8te3KbrGtMWqSJPa//4tHpptmMAbIhT38wA+QmJejkNGQ1k9pqHghH7/9ZpptT8w1 elvin@cli'
  elvin='ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCzopgAjYvdJrm/+d20NQOISqi9M36WFRy1HTuJCJE4LuimQApmkyUryCw9DpNgyKG0PqU4Asgt56UmEyBDSx2vYZOh+gcCuE0N7ldtGDm1UAeTtFssjJy/i72BNvBZNb6VHFSakMY+lhrfEv7z+H6cLuXO3ucVBt3XCVJy2PgD33x7fpclnjmHFqNuu2DSfGspnFj1WE3G3zav7q0SfWDfPMo4CTMxx7i+PVT5lGNCQoQWoSbSRgmzwzX0J/9Ezk8sU9BOP6zIxMCh0wfhAgXmNIAwzUaatD8A2atZ5dxl7EmnBLSTCz118n0AT0+pGm6MbQH/3AAcrWLXkqniiqkl Elvin.Xu'
  echo "${cli}" >>"${authorized_keys}"
  echo "${elvin}" >>"${authorized_keys}"
}
add_keys

install_python() {
  export DEBIAN_FRONTEND=noninteractive \
  && apt-get update \
  && apt-get -y \
    -o DPkg::options::="--force-confdef" \
    -o DPkg::options::="--force-confold" upgrade \
  && apt-get install -y python python-apt htop \
  && reboot
}
[[ -f /etc/debian_version ]] && { install_python ; }
