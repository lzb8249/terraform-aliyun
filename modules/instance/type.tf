

data "alicloud_images" "os" {
  most_recent = true
  owners      = "system"
  name_regex  = "^${var.os}"
}

