
output "host" {
  value = module.elvin_bastion.host
}

output "ip" {
  value = module.elvin_bastion.ip
}

output "public_ip" {
  value = data.terraform_remote_state.network.outputs.eip_elvin_bastion.ip
}
