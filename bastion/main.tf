module "elvin_bastion" {
  source            = "../modules/instance"
  environment       = terraform.workspace
  vpc_id            = data.terraform_remote_state.network.outputs.vpc.id
  vswitch_info      = data.terraform_remote_state.network.outputs.vswitch_info["${terraform.workspace}"]
  security_groups   = [data.terraform_remote_state.network.outputs.security_group.all, data.terraform_remote_state.network.outputs.security_group.ssh]
  service           = "elvin-bastion"
  instance_type     = data.alicloud_instance_types.c1g1.instance_types.0.id
  # instance_type     = "ecs.t5-lc1m1.small"
  system_disk_size  = "40"
  instance_count    = 1
  # os                = "centos_7"
  os                = "ubuntu_16"
  # image_id          = "centos_7_8_tpm_x64_20G_alibastion_20200730.vhd"
  tags              = map("item", "test")
}

#eip
resource "alicloud_eip_association" "elvin_bastion_public_ip" {
  allocation_id = data.terraform_remote_state.network.outputs.eip_elvin_bastion.id
  instance_id   = module.elvin_bastion.ids[count.index]
  count         = 1
}


# #eip test
# resource "alicloud_eip" "bastion_eip" {
#   name                 = "${terraform.workspace}-bastion_eip"
#   bandwidth            = 10
# }
# resource "alicloud_eip_association" "elvin_bastion_public_ip" {
#   allocation_id = alicloud_eip.bastion_eip.id
#   instance_id   = module.elvin_bastion.ids[count.index]
#   count         = 1
# }
# output "public_ip" {
#   value = alicloud_eip.bastion_eip.ip_address
# }
