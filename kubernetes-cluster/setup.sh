#!/bin/bash
add_keys() {
  ssh_dir='/root/.ssh'
  authorized_keys="${ssh_dir}/authorized_keys"
  mkdir -p ${ssh_dir}
  elvin='ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCzopgAjYvdJrm/+d20NQOISqi9M36WFRy1HTuJCJE4LuimQApmkyUryCw9DpNgyKG0PqU4Asgt56UmEyBDSx2vYZOh+gcCuE0N7ldtGDm1UAeTtFssjJy/i72BNvBZNb6VHFSakMY+lhrfEv7z+H6cLuXO3ucVBt3XCVJy2PgD33x7fpclnjmHFqNuu2DSfGspnFj1WE3G3zav7q0SfWDfPMo4CTMxx7i+PVT5lGNCQoQWoSbSRgmzwzX0J/9Ezk8sU9BOP6zIxMCh0wfhAgXmNIAwzUaatD8A2atZ5dxl7EmnBLSTCz118n0AT0+pGm6MbQH/3AAcrWLXkqniiqkl Elvin'
  echo "${cli}" >>"${authorized_keys}"
  echo "${elvin}" >>"${authorized_keys}"
}
add_keys
