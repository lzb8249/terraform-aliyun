
variable "availability_zone" {
    default = ["cn-shanghai-a","cn-shanghai-b","cn-shanghai-c"]
}

variable "master_instance_types" {
  default     = ["ecs.n1.large","ecs.n1.large","ecs.n1.large"]
}

variable "worker_instance_types" {
  default     = ["ecs.n1.large","ecs.n1.large"]
}

variable "password" {
  description = "The password of ECS instance."
  default     = "Just2test"
}

variable "worker_number" {
  default     = 3
}


variable "cluster_addons" {
    description = "Addon components in kubernetes cluster"

    type = list(object({
        name      = string
        config    = string
    }))

    default = [
        {
            "name"     = "flannel",
            "config"   = "",
        },
        {
            "name"     = "arms-prometheus",
            "config"   = "",
        },
        {
            "name"     = "csi-plugin",
            "config"   = "",
        },
        {
            "name"     = "csi-provisioner",
            "config"   = "",
        },
        {
            "name"     = "alicloud-disk-controller",
            "config"   = "",
        },
        {
            "name"     = "logtail-ds",
            "config"   = "{\"IngressDashboardEnabled\":\"true\"}",
        },
        {
            "name"     = "nginx-ingress-controller",
            "config"   = "{\"IngressSlbNetworkType\":\"internet\"}",
        },
    ]
}
