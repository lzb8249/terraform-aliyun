
# 名称
variable "name" {
  default = "elvin-k8s-demo-node"
}

# 节点ECS实例配置
data "alicloud_instance_types" "c2g4" {
  availability_zone    = "cn-shanghai-a"
  cpu_core_count       = 2
  memory_size          = 4
  kubernetes_node_role = "Worker"
}

#已创建的网络和安全组
locals {
  k8s_vswitch = data.terraform_remote_state.network.outputs.vswitch_info.k8s
}
locals {
  security_group = "${data.terraform_remote_state.network.outputs.security_group}"
}

# kubernetes托管版, 不单独创建master节点
resource "alicloud_cs_managed_kubernetes" "k8s_node" {
  # 集群名称
  name                      = var.name
  # ssh登录密码,必须指定password或key_name
  password                  = "K8s@2020"
  # 集群将位于的vswitch
  worker_vswitch_ids        = ["${local.k8s_vswitch.a}", "${local.k8s_vswitch.b}", "${local.k8s_vswitch.c}"]
  security_group_id         = local.security_group.all
  # 创建k8s集群时创建新的nat网关,默认为true
  new_nat_gateway           = false
  # 节点的ECS实例类型
  worker_instance_types     = [data.alicloud_instance_types.c2g4.instance_types[0].id]
  # kubernetes群集的总工作节点数。默认值为3。最大限制为50
  worker_number             = 2
  # pod网络的CIDR块。当cluster_network_type设置为flannel，必须设定该参数
  pod_cidr                  = "172.20.0.0/16"
  # 服务网络的CIDR块。不能与VPC CIDR相同，不能与VPC中的Kubernetes群集使用的CIDR相同，也不能在创建后进行修改。
  service_cidr              = "172.21.0.0/20"
  #节点安装云监控。
  install_cloud_monitor     = true
  # 为API Server创建Internet负载均衡,默认为false。
  slb_internet_enabled      = false
  # 节点的系统磁盘类别。其有效值为cloud_ssd和cloud_efficiency。默认为cloud_efficiency。
  worker_disk_category      = "cloud_efficiency"
  user_data                 = file("${path.module}/setup.sh")
} 



output "name" {
  value       = alicloud_cs_managed_kubernetes.k8s_node.*.name
}
output "version" {
  value       = alicloud_cs_managed_kubernetes.k8s_node.*.version
}
output "worker_nodes" {
  description = "List worker nodes of cluster."
  value       = [alicloud_cs_managed_kubernetes.k8s_node.*.worker_nodes]
}

#参考
#https://help.aliyun.com/document_detail/146138.html
